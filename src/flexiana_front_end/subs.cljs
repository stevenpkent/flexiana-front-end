(ns flexiana-front-end.subs
  (:require
   [re-frame.core :as rf]))

(rf/reg-sub
 ::name
 (fn [db]
   (:name db)))

(rf/reg-sub
 ::str1
 (fn [db _]
   (:str1 db)))

(rf/reg-sub
 ::str2
 (fn [db _]
   (:str2 db)))

(rf/reg-sub
 ::disable-button?
 (fn [_]
   [(rf/subscribe [::str1])
    (rf/subscribe [::str2])])
 (fn [ss]
   (or (some #(= "" %) ss)
       (some nil? ss))))

(rf/reg-sub
 ::result
 (fn [db _]
   (condp = (:api-result db)
     true  "There is a match"
     false "There is not a match"
     nil)))

