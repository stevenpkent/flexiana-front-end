(ns flexiana-front-end.views
  (:require
   [re-frame.core :as rf]
   [flexiana-front-end.subs :as subs]
   [flexiana-front-end.events :as events]))

(defn scramble []
  (let [str1            (rf/subscribe [::subs/str1])
        str2            (rf/subscribe [::subs/str2])
        disable-button? (rf/subscribe [::subs/disable-button?])
        result          (rf/subscribe [::subs/result])]
    (fn []
      [:div
       [:h1 "Scramble"]

       [:div
        "String 1: "
        [:input {:type      "text"
                 :value     @str1
                 :on-change #(rf/dispatch [::events/str1-change (-> % .-target .-value)])}]]

       [:div
        "String 2: "
        [:input {:type      "text"
                 :value     @str2
                 :on-change #(rf/dispatch [::events/str2-change (-> % .-target .-value)])}]]

       [:button
        {:disabled @disable-button?
         :on-click #(rf/dispatch [::events/scramble])}
        "Run Scramble"]

       [:div
        [:span {:style {:color :red}}
         @result]]])))

