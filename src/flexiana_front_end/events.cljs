(ns flexiana-front-end.events
  (:require
   [re-frame.core :as rf]
   [flexiana-front-end.db :as db]
   [day8.re-frame.http-fx]
   [ajax.core :as ajax]))

(rf/reg-event-db
 ::initialize-db
 (fn [_ _]
   db/default-db))

(rf/reg-event-fx
 ::scramble
 (fn [{:keys [db]} _]
   (let [url (str "http://localhost:8080/scramble?str1=" (:str1 db) "&str2=" (:str2 db))]
     {:http-xhrio {:uri url
                   :method :get
                   :timeout 10000
                   :response-format (ajax/json-response-format {:keywords? true})
                   :on-success [::scramble-success]
                   :on-failure [::scramble-failure]}})))

(rf/reg-event-db
 ::str1-change
 (fn [db [_ new-str1]]
   (assoc db :str1 new-str1)))

(rf/reg-event-db
 ::str2-change
 (fn [db [_ new-str2]]
   (assoc db :str2 new-str2)))

(rf/reg-event-db
 ::scramble-success
 (fn [db [_ result]]
   (assoc db :api-result result)))

(rf/reg-event-db
 ::scramble-failure
 (fn [db [_ result]]
   (assoc db :api-result result)))


